using Iterators
using Combinatorics

# represent the players as singleton types

abstract Player

type A <: Player
end

type B <: Player
end

type C <: Player
end

const players = [A(),B(),C()]
const grid_size = 0.1

# remove the empty set from the power set
const power_set = filter!(x->x≠Set{Player}(),
                          [Set(i) for i in subsets(players)])


function main()
    shapley_value = shapleyValue()

    grid = buildGrid(grid_size)

    pre_nucleolus = preNucleolus(grid)

    for player in players
        print(string("Player: $(typeof(player)) \n\t",
                     "Shapley Value: $(shapley_value[player]) \n\t",
                     "Pre-Nucleolus: $(pre_nucleolus[player])\n"))
    end
end


# return the value of each coalition

function value(S::Set{Player})
    if length(S) == 1
        return(0)
    elseif S == Set{Player}()
        return(0)
    elseif S == Set([A(), B()])
        return(30)
    elseif Set([A(), C()]) ⊆ S
        return(40)
    elseif S == Set([B(), C()])
        return(0)
    else
        error("undefined input")
    end
end

function value(S::Array{Player,1})
    set_S = Set(S)
    if (length(S) == length(set_S))
        return(value(set_S))
    else
        error("bad input")
    end
end


# return the excess of a coalition relative to a given allocation

function excess(S::Set{Player}, x) 
    allocation_value = 0
    
    for player in S
        allocation_value += x[player]
    end

    return(value(S) - allocation_value)
end

# return the set of xs in X which minimize f on x, up to an optionally
# specified tolerance

function argmin{T}(f::Function, X::AbstractArray{T,1}; tol=0.0)
    y = map(f, X)
    min_value = minimum(y)

    ismin(i) = (abs(y[i] - min_value) <= tol)

    min_indices = map(ismin, indices(y)[1])
    return(X[min_indices])
end

# return the marginal value of each member of an ordering, as a dict

function getMarginalValues(ordering)
    S = Set{Player}()
    marginal_values = Dict{Player,Float64}()
    for player in ordering
        marginal_set = union(S, Set([player]))
        marginal_values[player] = value(marginal_set) - value(S)
        S = marginal_set
    end
    return(marginal_values)
end

# return the Shapley Value of the specified game

function shapleyValue()
    marginal_values = []
    for ordering in permutations(players)
        marginal_values = push!(marginal_values, getMarginalValues(ordering))
    end

    sv = Dict()
    for player in players
        sv[player] = mean([mv[player] for mv in marginal_values])
    end
    return(sv)
end

# functions to recursively computs the prenucleolus

function preNucleolus(X)
    return(first(preNucleolus(X,1)))
end


function preNucleolus(X, i)
    if length(X) == 1
        return(X)
    else
        X = argmin(x->lexicalExcess(x,i), X)
        return(preNucleolus(X, i+1))
    end
end

# get the i-th excess value (in descending order) for a given
# allocation

function lexicalExcess(x, i)
    excesses = map(S->excess(S, x), power_set)
    return(sort(excesses, rev=true)[i])
end

# a series of probably-too-complex functions to create the simplex of
# possible allocations

function buildGrid(g_size)
    n_players = length(players)
    total_value = value(players)
    start = [[i] for i in 0:g_size:total_value]
    grid_list = nth(iterate(x->advanceGrid(x,g_size,n_players),start),n_players)
    grid = map(x->Dict(zip(players,x)),grid_list)
    return(grid)
end

function advanceGrid(previous_grid, g_size, n_players)
    return(cat(1, map(x->getAllocations(x,g_size,n_players), previous_grid)...))
end

# see the allocations of the people before you, and return all possible
# allocations for yourself

function getAllocations(others_allocation, g_size, n_players)
    total_value = value(players)
    
    value_remaining = total_value - sum(others_allocation)

    my_allocations = []

    
    if length(others_allocation) == n_players -1
        # if you're the last guy, take everything that's left
        my_allocations = [cat(1, [value_remaining], others_allocation)]
        
    else
        # otherwise create a list of allocations
        for my_value in 0.0:g_size:value_remaining
            my_allocations = push!(my_allocations,
                                   cat(1, [my_value], others_allocation))
        end
    end
    return(my_allocations)
end



